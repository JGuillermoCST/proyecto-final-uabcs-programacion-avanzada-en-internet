<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "José Guillermo";
        $user->last_name = "Castro";
        $user->email = "jova_17@alu.uabcs.mx";
        $user->password = bcrypt("secret");
        $user->role = 1;
        $user->save();

        $user = new User();
        $user->name = "Taylor";
        $user->last_name = "Swift";
        $user->email = "swift@universal.com";
        $user->password = bcrypt("secret");
        $user->role = 2;
        $user->save();
    }
}
