<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run() {

        $category = new Category();
        $category->cat_name = "Science";
        $category->cat_description = "Books of chemistry, biology, physics and more.";
        $category->save();

        $category = new Category();
        $category->cat_name = "History";
        $category->cat_description = "Books of universal history or specialized history in a certain country.";
        $category->save();

        $category = new Category();
        $category->cat_name = "Health";
        $category->cat_description = "Books about exercise, diets, healthy lifestyle, nutrition and more.";
        $category->save();

        $category = new Category();
        $category->cat_name = "Marketing";
        $category->cat_description = "Books about marketing, business, administration and many more.";
        $category->save();

        $category = new Category();
        $category->cat_name = "Politics";
        $category->cat_description = "Books of politics.";
        $category->save();

        $category = new Category();
        $category->cat_name = "Autobiography";
        $category->cat_description = "Books of autobiography.";
        $category->save();

        $category = new Category();
        $category->cat_name = "Kids";
        $category->cat_description = "Books for kids.";
        $category->save();
    }
}
