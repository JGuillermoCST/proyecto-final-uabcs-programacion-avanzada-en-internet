<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', 'RouteController@index')->name('dashboard');

Route::group(['middleware' => ['auth']], function () {
    
    Route::get('/books','BookController@index')->name('Books');
    Route::post('/books','BookController@store');
    Route::put('/books','BookController@update');
    Route::delete('/books/{book}','BookController@destroy');
    Route::get('/books/details/{book}','BookController@show');

    Route::get('/categories','CategoryController@index')->name('Categories');
    Route::post('/categories','CategoryController@store');
    Route::put('/categories','CategoryController@update');
    Route::delete('/categories/{category}','CategoryController@destroy');

    Route::get('/loans','LoanController@index')->name('Loans');
    Route::post('/loans','LoanController@store');
    Route::put('/loans','LoanController@update');
    Route::delete('/loans/{loan}','LoanController@destroy');
    Route::get('/loans/details/{loan}','LoanController@show');

    Route::get('/users','UserController@index')->name('Users');
    Route::post('/users','UserController@store');
    Route::put('/users','UserController@update');
    Route::delete('/users/{user}','UserController@destroy');
    Route::get('/users/details/{user}','UserController@show');
});