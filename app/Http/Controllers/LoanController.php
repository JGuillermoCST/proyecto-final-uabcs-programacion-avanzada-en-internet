<?php

namespace App\Http\Controllers;

use App\Models\Loan;
use App\Models\User;
use App\Models\Book;
use Illuminate\Http\Request;

use Auth;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $loans = null;

        if (Auth::user()->hasPermissionTo('view all loans')) {
            $loans = Loan::all();
        } else {
            $loans = Loan::Where('user_id','=',Auth::user()->id)->get();
        }

        
        $users = User::Where('role','<>',1)->get();
        $books = Book::all();

        return view('loans/index',compact('loans','users','books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        if ($loan = Loan::create($request->all())) {

            return redirect()->back()->with('success','Information stored correctly');
        }

        return redirect()->back()->with('error','Error on information');
    }

    /**
     * Display the specified loan.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        if ($id) {

            $loans = Loan::Where('id', $id)->get();
            $singleLoan = $loans[0];
            $users = User::Select('id','name','last_name')->Where('id', $singleLoan['user_id'])->get();
            $singleUser = $users[0];
            $books = Book::Select('id','title','isbn')->Where('id', $singleLoan['book_id'])->get();
            $singleBook = $books[0];

            //var_dump($books);

            return view('loans.details',compact('singleLoan','singleUser','singleBook'));
        }

        return response()->json(['message' => 'Error on book details retrieving','code' => '403']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function edit(Loan $loan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan) {
        
        $loan = Loan::find($request->id);
        
        if ($loan) {

            if ($loan->update($request->all())) {

                return redirect()->back()->with('success','Information updated correctly');
            }

            return redirect()->back()->with('error','Error on information');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan) {

        if ($loan) {

            if ($loan->delete()) {

                return response()->json(['message' => 'Successfully deleted','code' => '200']);
            }

            return response()->json(['message' => 'Error on deletion','code' => '401','data' => $loan ]);
        }

        return response()->json(['message' => 'Error on deletion','code' => '400']);
    }
}
