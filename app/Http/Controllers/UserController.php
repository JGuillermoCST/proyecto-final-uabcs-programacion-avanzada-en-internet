<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Loan;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $users = User::Where('id','<>',Auth::user()->id)->get();
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $user = $request->all();
        $user['password'] = bcrypt($user['password']);
        //var_dump($user);

        if (User::create($user)) {
            return redirect()->back()->with('success','Information stored correctly');
        } else {
            return redirect()->back()->with('error','Error on information');
        }
    }

    /**
     * Display the specified user.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        if ($id) {

            $users = User::Where('id', $id)->get();
            $singleUser = $users[0];
            $loans = Loan::Where('user_id', $id)->get();
            $books = Book::Select('id','title')->get();

            //var_dump($books);

            return view('users.details',compact('singleUser','loans','books'));
        }

        return response()->json(['message' => 'Error on book details retrieving','code' => '403']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user) {

        $user = User::find($request->id);

        if ($user) {
            
            if ($user->update($request->all())) {
                return redirect()->back()->with('success','Information updated correctly');
            }
            return redirect()->back()->with('error','Error on information');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        
        if ($user) {

            if ($user->delete()) {

                return response()->json(['message' => 'Successfully deleted','code' => '200']);
            }
            return response()->json(['message' => 'Error on deletion','code' => '401','data' => $user]);
        }
        return response()->json(['message' => 'Error on deletion','code' => '400']);
    }
}
