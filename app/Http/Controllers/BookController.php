<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Category;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Http\Request;

use Auth;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        $categories = Category::all();
        return view('books.index',compact('books','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($book = Book::create($request->all())) {

            if ($request->hasFile('cover')) {
                $file = $request->file('cover');
                $file_name = 'book_cover_' . $book->id .'.' . $file->getClientOriginalExtension();

                $path = $request->file('cover')->storeAs('public/img/books',$file_name);

                $book->cover = $file_name;
                $book->save();
            }

            return redirect()->back()->with('success','Information stored correctly');
        }
    }

    /**
     * Display the specified book.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id) {

            $loans = null;

            $books = Book::Where('id', $id)->get();
            $singleBook = $books[0];
            $categories = Category::Where('id', $singleBook->category_id)->get();
            $category = $categories[0];
            if (Auth::user()->hasPermissionTo('view all loans')) {
                $loans = Loan::Where('book_id',$id)->get();
            } else {
                $loans = Loan::Where('user_id',Auth::user()->id)->get();
            }
            $users = User::select('id','name','last_name')->get();

            //var_dump($singleBook);

            return view('books.details',compact('singleBook','category','loans','users'));
        }
        return response()->json(['message' => 'Error on book details retrieving','code' => '403']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book) {

        $book = Book::find($request->id);

        if ($book) {
            
            if ($book->update($request->all())) {

                if ($request->hasFile('cover')) {
                    $file = $request->file('cover');
                    $file_name = 'book_cover_' . $book->id .'.' . $file->getClientOriginalExtension();
    
                    $path = $request->file('cover')->storeAs('public/img/books',$file_name);
    
                    $book->cover = $file_name;
                    $book->save();
                }

                return redirect()->back()->with('success','Information updated correctly');
            }

            return redirect()->back()->with('error','Error on information');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book) {
        
        if ($book) {

            if ($book->delete()) {

                return response()->json(['message' => 'Successfully deleted','code' => '200']);
            }

            return response()->json(['message' => 'Error on deletion','code' => '401','data' => $book ]);
        }

        return response()->json(['message' => 'Error on deletion','code' => '400']);
    }
}
