<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-md-8 col-12 mt-2">
                <h2 class="font-semibold text-xl text-gray-200 leading-tight">
                    {{ __('Books/Details/ '. $singleBook->title) }}
                </h2>
            </div>
        </div>
    </x-slot>

    <div class="py-12 bg-gray-900">
        <div class="max-w-8xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden shadow-xl sm:rounded-lg bg-gray-900 row">

                <div class="card col-3 p-0 border-0 bg-transparent">
                    <img class="card-img-top" src="{{ asset('storage/img/books/'. $singleBook->cover .'') }}" alt="Book image cap">
                </div>

                <div class="card col ml-2 p-0">
                    <div class="card-body pl-0 pr-0">
                        <h5 class="card-title pl-3 text-3xl text-center"><strong>Information</strong></h5>
                        <ul class="list-group list-group-flush text-xl">
                            <li class="list-group-item"><strong>Title:</strong> {{ $singleBook->title }}</li>
                            <li class="list-group-item"><strong>Description:</strong> {{ $singleBook->description }}</li>
                            <li class="list-group-item"><strong>Year:</strong> {{ $singleBook->year }}</li>
                            <li class="list-group-item"><strong>Pages:</strong> {{ $singleBook->pages }}</li>
                            <li class="list-group-item"><strong>ISBN:</strong> {{ $singleBook->isbn }}</li>
                            <li class="list-group-item"><strong>Editorial:</strong> {{ $singleBook->editorial }}</li>
                            <li class="list-group-item"><strong>Edition:</strong> {{ $singleBook->edition }}</li>
                            <li class="list-group-item"><strong>Author:</strong> {{ $singleBook->author }}</li>
                            <li class="list-group-item"><strong>Category:</strong> {{ $category->cat_name }}</li>
                            <li class="list-group-item"></li>
                        </ul>
                        <h5 class="card-title pl-3 text-3xl text-bold text-center"><strong>Loans</strong></h5>
                        <table class="table table-responsive table-light table-striped table-bordered d-table">
                            <thead class="thead-dark">
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">User</th>
                                <th scope="col">Loan date</th>
                                <th scope="col">Status</th>
                              </tr>
                            </thead>
                            <tbody>
                                @if (isset($loans) && count($loans)>0)
                                @foreach ($loans as $l)  
                                <tr>
                                    @php
                                        $user = "";

                                        foreach ($users as $u) {
                                            if ($u->id == $l->user_id) {
                                                $user = $u->name . ' ' . $u->last_name;
                                            }
                                        }
                                    @endphp
        
                                    <th scope="row"><a href="{{ url('/loans/details/'. $l->id . '') }}" class="text-blue-800"> {{$l->id}} </a></th>
                                    <td>
                                        @if (Auth::user()->hasPermissionTo('view users'))
                                            <a href="{{ url('/users/details/'. $l->user_id . '') }}" class="text-blue-800">{{$user}}</a>
                                        @else
                                            {{$user}}
                                        @endif
                                    </td>
                                    <td>{{$l->loan_date}}</td>
                                    <td>{{$l->status}}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>