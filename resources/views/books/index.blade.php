<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-md-8 col-12 mt-2">
                <h2 class="font-semibold text-xl text-gray-200 leading-tight">
                    {{ __('Books') }}
                </h2>
            </div>
            @if (Auth::user()->hasPermissionTo('update books'))
            <div class="col-md-4">
                <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addBookModal">
                    Add book
                </button>
            </div>
            @endif
        </div>
    </x-slot>

    <div class="py-12 bg-gray-900">
        <div class="max-w-8xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden shadow-xl sm:rounded-lg bg-gray-900">
                
                <table class="table table-responsive table-light table-striped table-bordered d-table">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th class="isbn" scope="col">ISBN</th>
                        <th class="editorial" scope="col">Editorial</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if (isset($books) && count($books)>0)
                        @foreach ($books as $book)  
                        <tr>
                            <th scope="row"> {{$book->id}} </th>
                            <td>{{$book->title}}</td>
                            <td class="isbn">{{$book->isbn}}</td>
                            <td class="editorial">{{$book->editorial}}</td>
                            <td>
                                <a class="btn btn-success" href="{{ url('books/details/'.$book->id.'') }}">Book details</a>
                                @if (Auth::user()->hasPermissionTo('update books'))
                                <button class="btn btn-warning" data-toggle="modal" data-target="#editBookModal" onclick="editBook('{{$book->id}}','{{$book->title}}','{{$book->description}}','{{$book->year}}','{{$book->pages}}','{{$book->isbn}}','{{$book->editorial}}','{{$book->edition}}','{{$book->author}}',{{$book->category_id}})">Edit Book</button>
                                @endif
                                @if (Auth::user()->hasPermissionTo('delete books'))
                                <button class="btn btn-danger" onclick="removeBook('{{$book->id}}',this)">Remove Book</button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>

    <!-- Add book modal -->
    <div class="modal fade" id="addBookModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add new book</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('books') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <!-- TITLE -->
                            <div class="form-group col-md-7">
                                <label for="title">Title</label>
                                <input type="text" class="form-control form-control-md" name="title" id="title" placeholder="Book title">
                            </div>
                            <!-- TITLE -->
                            <div class="form-group col-md-5">
                                <label for="category_id">Category</label>
                                <select class="custom-select" name="category_id" id="category_id">
                                    @if (isset($categories) && count($categories) > 0)
                                        @foreach ($categories as $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->cat_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <!-- DESCRIPTION -->
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control form-control-md" name="description" id="description" cols="5" placeholder="Book description"></textarea>
                        </div>
                        <div class="form-row">
                            <!-- YEAR -->
                            <div class="form-group col-md-4">
                                <label for="year">Year</label>
                                <input type="number" class="form-control form-control-md" name="year" id="year" placeholder="Book year">
                            </div>
                            <!-- PAGES -->
                            <div class="form-group col-md-4">
                                <label for="pages">Pages</label>
                                <input type="number" class="form-control form-control-md" name="pages" id="pages" placeholder="Book pages">
                            </div>
                            <!-- ISBN -->
                            <div class="form-group col-md-4">
                                <label for="isbn">ISBN</label>
                                <input type="text" class="form-control form-control-md" name="isbn" id="isbn" placeholder="Book ISBN">
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- EDITION -->
                            <div class="form-group col-md-2">
                                <label for="edition">Edition</label>
                                <input type="number" class="form-control form-control-md" name="edition" id="edition" placeholder="Book edition">
                            </div>
                            <!-- AUTHOR -->
                            <div class="form-group col-md-10">
                                <label for="author">Author</label>
                                <input type="text" class="form-control form-control-md" name="author" id="author" placeholder="Book author">
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- EDITORIAL -->
                            <div class="form-group col-md-8">
                                <label for="editorial">Editorial</label>
                                <input type="text" class="form-control form-control-md" name="editorial" id="editorial" placeholder="Book editorial">
                            </div>
                            <!-- FILE -->
                            <div class="form-group col-md-4">
                                <label for="cover">Cover</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="cover" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save new book</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit book modal -->
    <div class="modal fade" id="editBookModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit book</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('books') }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="modal-body">
                        <div class="form-row">
                            <!-- TITLE -->
                            <div class="form-group col-md-7">
                                <label for="title">Title</label>
                                <input id="bTitle" type="text" class="form-control form-control-md" name="title" id="title" placeholder="Book title">
                            </div>
                            <!-- CATEGORY -->
                            <div class="form-group col-md-5">
                                <label for="category_id">Category</label>
                                <select id="bCategory" class="custom-select" name="category_id" id="category_id">
                                    @if (isset($categories) && count($categories) > 0)
                                        @foreach ($categories as $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->cat_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <!-- DESCRIPTION -->
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="bDescription" class="form-control form-control-md" name="description" id="description" cols="5" placeholder="Book description"></textarea>
                        </div>
                        <div class="form-row">
                            <!-- YEAR -->
                            <div class="form-group col-md-4">
                                <label for="year">Year</label>
                                <input id="bYear" type="number" class="form-control form-control-md" name="year" id="year" placeholder="Book year">
                            </div>
                            <!-- PAGES -->
                            <div class="form-group col-md-4">
                                <label for="pages">Pages</label>
                                <input id="bPages" type="number" class="form-control form-control-md" name="pages" id="pages" placeholder="Book pages">
                            </div>
                            <!-- ISBN -->
                            <div class="form-group col-md-4">
                                <label for="isbn">ISBN</label>
                                <input id="bIsbn" type="text" class="form-control form-control-md" name="isbn" id="isbn" placeholder="Book ISBN">
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- EDITION -->
                            <div class="form-group col-md-2">
                                <label for="edition">Edition</label>
                                <input id="bEdition" type="number" class="form-control form-control-md" name="edition" id="edition" placeholder="Book edition">
                            </div>
                            <!-- AUTHOR -->
                            <div class="form-group col-md-10">
                                <label for="author">Author</label>
                                <input id="bAuthor" type="text" class="form-control form-control-md" name="author" id="author" placeholder="Book author">
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- EDITORIAL -->
                            <div class="form-group col-md-8">
                                <label for="editorial">Editorial</label>
                                <input id="bEditorial" type="text" class="form-control form-control-md" name="editorial" id="editorial" placeholder="Book editorial">
                            </div>
                            <!-- FILE -->
                            <div class="form-group col-md-4">
                                <label for="cover">Cover</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="cover" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Edit book</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <x-slot name="scripts" >
        <script type="text/javascript">

            function editBook(id,title,description,year,pages,isbn,editorial,edition,author,category) {

                //console.log(id)

                $('#id').val(id)
                $('#bTitle').val(title)
                $('#bDescription').val(description)
                $('#bYear').val(year)
                $('#bPages').val(pages)
                $('#bIsbn').val(isbn)
                $('#bEditorial').val(editorial)
                $('#bEdition').val(edition)
                $('#bAuthor').val(author)
                $('#bCategory').val(category)
            }

            function removeBook(id,target) {

                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this book!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {

                        axios.delete('{{ url('books') }}/'+id, {
                            id: id,
                            _token: '{{ csrf_token() }}',
                        })
                        .then(function (response) {

                            if (response.data.code == 200) {
                                swal("Poof! Your book has been deleted!", {icon: "success",})

                                $(target).parent().parent().remove()
                            } else {
                                swal("Error ocurred, not success", {icon: "error",})
                            }
                        })
                        .catch(function (error) {
                            swal("Error ocurred", {icon: "error",})
                        })
                    } else {
                        swal("Your book is safe!");
                    }
                });
            }
        </script>
    </x-slot>
</x-app-layout>
