<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-md-8 col-12 mt-2">
                <h2 class="font-semibold text-xl text-gray-200 leading-tight">{{ __('Loans/Details/ '. $singleLoan->id) }}</h2>
            </div>
        </div>
    </x-slot>

    <div class="py-12 bg-gray-900">
        <div class="max-w-8xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden shadow-xl sm:rounded-lg bg-gray-900 row">

                <div class="card col p-0">
                    <div class="card-body pl-0 pr-0">
                        <h5 class="card-title pl-3 text-3xl text-bold text-center"><strong>Information</strong></h5>
                        <ul class="list-group list-group-flush text-xl">
                            <li class="list-group-item"><strong>Client: </strong>
                                @if (Auth::user()->hasPermissionTo('view users'))
                                <a href="{{ url('/users/details/'. $singleUser->id)}}" class="text-blue-800">{{ $singleUser->name . ' ' . $singleUser->last_name }}</a>
                                @else
                                {{ $singleUser->name . ' ' . $singleUser->last_name }}
                                @endif
                            </li>
                            <li class="list-group-item"><strong>Book: </strong>
                                <a href="{{ url('/books/details/'. $singleBook->id)}}" class="text-blue-800">{{ $singleBook->title . ' - ' . $singleBook->isbn }}</a>
                            </li>
                            <li class="list-group-item"><strong>Loan date:</strong> {{ $singleLoan->loan_date }} </li>
                            <li class="list-group-item"><strong>Return date:</strong> {{ $singleLoan->return_date }} </li>
                            <li class="list-group-item"><strong>Status:</strong> {{ $singleLoan->status }} </li>
                            <li class="list-group-item"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>