<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-md-8 col-12 mt-2">
                <h2 class="font-semibold text-xl text-gray-200 leading-tight">
                    {{ __('Loans') }}
                </h2>
            </div>
            @if (Auth::user()->hasPermissionTo('add loans'))
            <div class="col-md-4 col-12">
                <button class="btn btn-primary float-right" id="addLoan" data-toggle="modal" data-target="#addLoanModal">
                    Add Loan
                </button>
            </div>
            @endif
        </div>
    </x-slot>

    <div class="py-12 bg-gray-900">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-gray-900 overflow-hidden shadow-xl sm:rounded-lg">

                <table class="table table-responsive table-light table-striped table-bordered d-table">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">User</th>
                        <th scope="col">Book</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if (isset($loans) && count($loans)>0)
                        @foreach ($loans as $l)  
                        <tr>
                            @php
                                $uname = "";
                                $book = "";
                        
                                foreach ($users as $u) {
                                    if ($u->id == $l->user_id) {
                                        $uname = $u->name . ' ' . $u->last_name;
                                    }
                                }

                                foreach ($books as $b) {
                                    if ($b->id == $l->book_id) {
                                        $book = $b->isbn;
                                    }
                                }
                            @endphp

                            <th scope="row"> {{$l->id}} </th>
                            <td>
                                @if (Auth::user()->hasPermissionTo('view users'))
                                <a href="{{ url('/users/details/'. $l->user_id . '') }}" class="text-blue-800">{{$uname}}</a>
                                @else
                                {{$uname}}
                                @endif
                            </td>
                            <td><a href="{{ url('/books/details/'. $l->book_id . '') }}" class="text-blue-800">{{$book}}</a></td>
                            <td>{{$l->status}}</td>
                            <td>
                                <a class="btn btn-success" href="{{ url('loans/details/'.$l->id.'') }}">Loan details</a>
                                @if (Auth::user()->hasPermissionTo('update loans'))
                                <button class="btn btn-warning" data-toggle="modal" data-target="#editLoanModal" onclick="editLoan('{{$l->id}}','{{$l->user_id}}','{{$l->book_id}}','{{$l->loan_date}}','{{$l->status}}')">Edit Loan</button>
                                @endif
                                @if (Auth::user()->hasPermissionTo('delete loans'))
                                <button class="btn btn-danger" onclick="removeLoan('{{$l->id}}',this)">Remove Loan</button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Add Loan Modal -->
    <div class="modal fade" id="addLoanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Loan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form" method="POST" action="{{ url('loans') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="user">User</label>
                            <select type="text" name="user_id" class="form-control" id="user">
                                @if (isset($users) && count($users) > 0)
                                    @foreach ($users as $u)
                                        <option value="{{ $u->id }}">{{ $u->name . ' ' . $u->last_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="book">Book</label>
                            <select class="form-control" name="book_id" id="book">
                                @if (isset($books) && count($books) > 0)
                                    @foreach ($books as $b)
                                        <option value="{{ $b->id }}">{{ $b->title . ' - ' . $b->isbn }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="loan">Loan date</label>
                            <input type="text" class="form-control" name="loan_date" id="loan" min="2020-12-01">
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <input type="text" class="form-control" name="status" id="status" value="NO DEVUELTO" readonly>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add loan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Loan Modal -->
    <div class="modal fade" id="editLoanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit loan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form" method="POST" action="{{ url('loans') }}">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="luser">User</label>
                            <select type="text" name="user_id" class="form-control" id="luser">
                                @if (isset($users) && count($users) > 0)
                                    @foreach ($users as $u)
                                        <option value="{{ $u->id }}">{{ $u->name . ' ' . $u->last_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="lbook">Book</label>
                            <select class="form-control" name="book_id" id="lbook">
                                @if (isset($books) && count($books) > 0)
                                    @foreach ($books as $b)
                                        <option value="{{ $b->id }}">{{ $b->title . ' - ' . $b->isbn }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="eloan">Loan date</label>
                            <input type="text" class="form-control" name="loan_date" id="eloan" readonly>
                        </div>
                        <div class="form-group">
                            <label for="ereturn">Return date</label>
                            <input type="text" class="form-control" name="return_date" id="ereturn">
                        </div>
                        <div class="form-group">
                            <label for="lstatus">Status</label>
                            <select class="form-control" name="status" id="lstatus" value="NO DEVUELTO">
                                <option value="NO DEVUELTO">NO DEVUELTO</option>
                                <option value="DEVUELTO">DEVUELTO</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Edit loan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <x-slot name="scripts" >

        <script type="text/javascript">

            $(document).ready(function(){
                
                $('#loan').datepicker({
                    dateFormat: "yy-mm-dd",
                    minDate: -1,
                    maxDate: 0
                });
            });

            function editLoan(id,user,book,init,status) {

                console.log('entered in function')

                $('#luser').val(user)
                $('#lbook').val(book)
                $('#eloan').val(init)
                $('#lstatus').val(status)
                $('#id').val(id)

                $('#ereturn').datepicker({
                    dateFormat: "yy-mm-dd",
                    minDate: new Date(init),
                    maxDate: +1
                });
            }

            function removeLoan(id,target) {

                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this loan!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {

                        axios.delete('{{ url('loans') }}/'+id, {
                            id: id,
                            _token: '{{ csrf_token() }}',
                        })
                        .then(function (response) {

                            if (response.data.code == 200) {
                                swal("Poof! The loan has been deleted!", {icon: "success",})

                                $(target).parent().parent().remove()
                            } else {
                                swal("Error ocurred, not success", {icon: "error",})
                            }
                        })
                        .catch(function (error) {
                            swal("Error ocurred", {icon: "error",})
                        })
                    } else {
                        swal("The loan is safe!");
                    }
                });
            }
        </script>
    </x-slot>
</x-app-layout>