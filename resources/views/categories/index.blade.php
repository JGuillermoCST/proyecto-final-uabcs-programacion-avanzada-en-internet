<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-md-8 col-12 mt-2">
                <h2 class="font-semibold text-xl text-gray-200 leading-tight">
                    {{ __('Categories') }}
                </h2>
            </div>
            <div class="col-md-4 col-12">
                <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addCategoryModal">
                    Add Category
                </button>
            </div>
        </div>
    </x-slot>

    <div class="py-12 bg-gray-900">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-gray-900 overflow-hidden shadow-xl sm:rounded-lg">
    
                <table class="table table-striped table-bordered table-light">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th class="description" scope="col">Description</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if (isset($categories) && count($categories)>0)
                        @foreach ($categories as $category)  
                        <tr>
                            <th scope="row">{{$category->id}}</th>
                            <td>{{$category->cat_name}}</td>
                            <td class="description">{{$category->cat_description}}</td>
                            <td>
                                <button class="btn btn-warning" data-toggle="modal" data-target="#editCategoryModal" onclick="editCategory('{{$category->id}}','{{$category->cat_name}}','{{$category->cat_description}}')">Edit Category</button>
                                <button class="btn btn-danger" onclick="removeCategory('{{$category->id}}',this)">Remove Category</button>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>

    <!-- Add Category Modal -->
    <div class="modal fade" id="addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('categories') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="cat_name">Title</label>
                            <input type="text" name="cat_name" class="form-control" id="cat_name" placeholder="Insert title">
                          </div>
                        <div class="form-group">
                            <label for="cat_description">Description</label>
                            <textarea class="form-control" name="cat_description" id="cat_description" rows="4" placeholder="Category description"></textarea>
                          </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Category Modal -->
    <div class="modal fade" id="editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('categories') }}">
                    @csrf
                    @method('PUT')

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="uName">Title</label>
                            <input type="text" name="cat_name" class="form-control" id="uName" placeholder="Insert title">
                          </div>
                        <div class="form-group">
                            <label for="uDescription">Description</label>
                            <textarea class="form-control" name="cat_description" id="uDescription" rows="4" placeholder="Category description"></textarea>
                          </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <x-slot name="scripts" >

        <script type="text/javascript">

            function editCategory(id,name,description) {

                console.log('entered in function')

                $('#uName').val(name)
                $('#uDescription').val(description)
                $('#id').val(id)
            }

            function removeCategory(id,target) {

                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this registry!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {

                        axios.delete('{{ url('categories') }}/'+id, {
                            id: id,
                            _token: '{{ csrf_token() }}',
                        })
                        .then(function (response) {

                            if (response.data.code == 200) {
                                swal("Poof! Your registry has been deleted!", {icon: "success",})

                                $(target).parent().parent().remove()
                            } else {
                                swal("Error ocurred, not success", {icon: "error",})
                            }
                        })
                        .catch(function (error) {
                            swal("Error ocurred", {icon: "error",})
                        })
                    } else {
                        swal("Your registry is safe!");
                    }
                });
            }
        </script>
    </x-slot>
</x-app-layout>
