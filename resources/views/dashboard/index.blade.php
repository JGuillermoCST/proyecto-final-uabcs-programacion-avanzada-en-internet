<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-md-8 col-12 mt-2">
                <h2 class="font-semibold text-xl text-gray-200 leading-tight">
                    {{ __('My dashboard') }}
                </h2>
            </div>
        </div>
    </x-slot>

    <div class="py-12 bg-gray-900">
        <div class="max-w-8xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden shadow-xl sm:rounded-lg bg-gray-900">

                <div class="p-6 sm:px-20 bg-dark border-b border-gray-200">
                    <div>
                        <x-jet-application-logo class="block h-12 w-auto" />
                    </div>
                
                    <div class="mt-8 text-2xl text-white">
                        Bienvenido al sitema de biblioteca!
                    </div>

                    @if (Auth::user()->hasPermissionTo('view all loans'))

                        <img src="{{ asset('storage/img/banner/library-banner.png') }}" class="img-fluid mt-3" alt="Responsive image">
                        
                        <div class="mt-8 text-2xl text-white">
                            Últimos prestamos hechos
                        </div>

                        <table class="table table-responsive table-light table-striped table-bordered d-table">
                            <thead class="thead-dark">
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">User</th>
                                <th scope="col">Book</th>
                                <th scope="col">Status</th>
                              </tr>
                            </thead>
                            <tbody>
                                @if (isset($loans) && count($loans)>0)
                                @php
                                   $count = 0;     
                                @endphp 
                                    @foreach ($loans as $l)
                                    @php
                                        $count += 1;
                                    @endphp  
                                        @if ($count < 3)
                                            <tr>
                                                @php
                                                    $uname = "";
                                                    $book = "";
                                            
                                                    foreach ($users as $u) {
                                                        if ($u->id == $l->user_id) {
                                                            $uname = $u->name . ' ' . $u->last_name;
                                                        }
                                                    }
                    
                                                    foreach ($books as $b) {
                                                        if ($b->id == $l->book_id) {
                                                            $book = $b->isbn;
                                                        }
                                                    }
                                                @endphp
                    
                                                <th scope="row"> {{$l->id}} </th>
                                                <td>
                                                    @if (Auth::user()->hasPermissionTo('view users'))
                                                    <a href="{{ url('/users/details/'. $l->user_id . '') }}" class="text-blue-800">{{$uname}}</a>
                                                    @else
                                                    {{$uname}}
                                                    @endif
                                                </td>
                                                <td><a href="{{ url('/books/details/'. $l->book_id . '') }}" class="text-blue-800">{{$book}}</a></td>
                                                <td>{{$l->status}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    @else
                        <div class="mt-6 text-gray-400">
                        Aquí podrás ver los libros disponibles y todos los préstamos que hayas solicitado. Para pedir prestado un libro, guarda el isbn y acude con el bibliotecario para solicitarlo.
                        </div>

                        <img src="{{ asset('storage/img/banner/library-banner.png') }}" class="img-fluid mt-3" alt="Responsive image">
                        
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>