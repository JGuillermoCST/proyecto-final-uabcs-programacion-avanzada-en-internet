<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-md-8 col-12 mt-2">
                <h2 class="font-semibold text-xl text-gray-200 leading-tight">{{ __('Users/Details/ '. $singleUser->name . ' ' . $singleUser->last_name) }}</h2>
            </div>
        </div>
    </x-slot>

    <div class="py-12 bg-gray-900">
        <div class="max-w-8xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden shadow-xl sm:rounded-lg bg-gray-900 row">

                <div class="card col-3 p-0 border-0 bg-transparent">
                    <img class="card-img-top" src="{{ asset('storage/'. $singleUser->profile_photo_path .'') }}" alt="User image">
                </div>

                <div class="card col ml-2 p-0">
                    <div class="card-body pl-0 pr-0">
                        <h5 class="card-title pl-3 text-3xl text-bold"><strong>Information</strong></h5>
                        <ul class="list-group list-group-flush text-xl">
                            <li class="list-group-item"><strong>Name:</strong> {{ $singleUser->name . ' ' . $singleUser->last_name }}</li>
                            <li class="list-group-item"><strong>Email:</strong> {{ $singleUser->email }}</li>
                            <li class="list-group-item"><strong>Role:</strong> 
                                @if ($singleUser->role == 1)
                                    Client
                                @else
                                    Administrator
                                @endif
                            </li>
                            <li class="list-group-item"></li>
                        </ul>
                        <h5 class="card-title pl-3 text-3xl text-bold"><strong>Loans</strong></h5>

                        <table class="table table-responsive table-light table-striped table-bordered d-table">
                            <thead class="thead-dark">
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">Book</th>
                                <th scope="col">Status</th>
                              </tr>
                            </thead>
                            <tbody>
                                @if (isset($loans) && count($loans)>0)
                                @foreach ($loans as $l)  
                                <tr>
                                    @php
                                        $book = "";

                                        foreach ($books as $b) {
                                            if ($b->id == $l->book_id) {
                                                $book = $b->title;
                                            }
                                        }
                                    @endphp
        
                                    <th scope="row"><a href="{{ url('/loans/details/'. $l->id . '') }}" class="text-blue-800"> {{$l->id}} </a></th>
                                    <td><a href="{{ url('/books/details/'. $l->book_id . '') }}" class="text-blue-800">{{$book}}</a></td>
                                    <td>{{$l->status}}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>