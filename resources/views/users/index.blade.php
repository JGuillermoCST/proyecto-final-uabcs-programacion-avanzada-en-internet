<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-md-8 col-12 mt-2">
                <h2 class="font-semibold text-xl text-gray-200 leading-tight">{{ __('Users') }}</h2>
            </div>
            <div class="col-md-4">
                <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addUserModal">Add user</button>
            </div>
        </div>
    </x-slot>

    <div class="py-12 bg-gray-900">
        <div class="max-w-8xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden shadow-xl sm:rounded-lg bg-gray-900">

                <table class="table table-responsive table-light table-striped table-bordered d-table">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th class="lname" scope="col">Last name</th>
                        <th class="email" scope="col">Email</th>
                        <th class="role" scope="col">Role</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if (isset($users) && count($users)>0)
                        @foreach ($users as $user)  
                        <tr>
                            <th scope="row"> {{$user->id}} </th>
                            <td>{{$user->name}}</td>
                            <td class="lname">{{$user->last_name}}</td>
                            <td class="email">{{$user->email}}</td>
                            @if ($user->role == 1)
                                <td class="email">Client</td>
                            @else
                                <td class="email">Administrator</td>
                            @endif
                            <td>
                                <a class="btn btn-success" href="{{ url('users/details/'.$user->id.'') }}">User details</a>
                                <button class="btn btn-warning" data-toggle="modal" data-target="#editUserModal" onclick="editUser('{{$user->id}}','{{$user->name}}','{{$user->last_name}}','{{$user->email}}','{{$user->role}}')">Edit User</button>
                                <button class="btn btn-danger" onclick="removeUser('{{$user->id}}',this)">Remove User</button>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Add user modal -->
    <div class="modal fade" id="addUserModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add new user</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('users') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <!-- NAME -->
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control form-control-md" name="name" id="name" placeholder="User name">
                        </div>
                        <!-- LAST NAME -->
                        <div class="form-group">
                            <label for="last_name">Last name</label>
                            <input type="text" class="form-control form-control-md" name="last_name" id="last_name" placeholder="User last name" required>
                        </div>
                        <!-- EMAIL -->
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control form-control-md" name="email" id="email" placeholder="User email" required>
                        </div>
                        <!-- PASSWORD -->
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" class="form-control form-control-md" name="password" id="password" value="abcd1234" readonly>
                        </div>
                        <!-- ROLE -->
                        <div class="form-group">
                            <label for="role">Role</label>
                            <select class="form-control form-control-md" name="role" id="role">
                                <option value="1" selected>Client</option>
                                <option value="2">Administrator</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save new user</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Update user modal -->
    <div class="modal fade" id="editUserModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit user</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('users') }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="modal-body">
                        <!-- NAME -->
                        <div class="form-group">
                            <label for="uName">Name</label>
                            <input type="text" class="form-control form-control-md" name="name" id="uName" required>
                        </div>
                        <!-- LAST NAME -->
                        <div class="form-group">
                            <label for="uLastName">Last name</label>
                            <input type="text" class="form-control form-control-md" name="last_name" id="uLastName" required>
                        </div>
                        <!-- EMAIL -->
                        <div class="form-group">
                            <label for="e=uEmail">Email</label>
                            <input type="text" class="form-control form-control-md" name="email" id="uEmail" required>
                        </div>
                        <!-- ROLE -->
                        <div class="form-group">
                            <label for="uRole">Role</label>
                            <select class="form-control form-control-md" name="role" id="uRole">
                                <option value="1">Client</option>
                                <option value="2">Administrator</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save new user</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <x-slot name="scripts" >
        <script type="text/javascript">

            function editUser(id,name,lname,email,role) {

                //console.log(id)

                $('#id').val(id)
                $('#uName').val(name)
                $('#uLastName').val(lname)
                $('#uEmail').val(email)
                $('#uRole').val(role)
            }

            function removeUser(id,target) {

                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this user!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {

                        axios.delete('{{ url('users') }}/'+id, {
                            id: id,
                            _token: '{{ csrf_token() }}',
                        })
                        .then(function (response) {

                            if (response.data.code == 200) {
                                swal("Poof! The user has been deleted!", {icon: "success",})

                                $(target).parent().parent().remove()
                            } else {
                                swal("Error ocurred, not success", {icon: "error",})
                            }
                        })
                        .catch(function (error) {
                            swal("Error ocurred", {icon: "error",})
                        })
                    } else {
                        swal("The user is safe!");
                    }
                });
            }
        </script>
    </x-slot>
</x-app-layout>